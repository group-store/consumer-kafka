package com.kotlin.store.service

import com.kotlin.store.entity.Body
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service

@Service
class SendEmailService {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(CustomerEventsService::class.java)
    }

    @Autowired
    private lateinit var mailSender: JavaMailSender

    fun sendEmail(body: Body) {
        val message = SimpleMailMessage()
        message.from = "mayapologies6@gmail.com"
        message.setTo(body.to)
        message.subject = body.subject
        message.text = body.text
        mailSender.send(message)
        logger.info("Message sent successfully! to ${body.to}")
    }

}