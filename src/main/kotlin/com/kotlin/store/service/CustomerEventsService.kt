package com.kotlin.store.service

import com.kotlin.store.entity.Body
import com.kotlin.store.events.Event
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class CustomerEventsService {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(CustomerEventsService::class.java)
    }

    @Autowired
    private lateinit var sendEmailService: SendEmailService

    @KafkaListener(
        topics = ["\${topic.customer.name:customers}"],
        containerFactory = "kafkaListenerContainerFactory",
        groupId = "group1"
    )
    fun consumer(event: Event) {
        logger.info("""
            MESSAGE received customer created: 
            ID: ${event.id}
            DATE: ${event.date}
            TYPE: ${event.type}
            DATA: ${event.data}
        """.trimIndent())
        val body = Body("mayapologies@gmail.com",
            event.data.email,
            "Bienvenido!",
            """Hola ${event.data.name}! 
               Acaba de suscribirse a nuestro servicio""".trimMargin())
        sendEmailService.sendEmail(body)
    }


}