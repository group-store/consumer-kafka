package com.kotlin.store.entity

enum class PersonType {
    CLIENT,
    PROVIDER
}