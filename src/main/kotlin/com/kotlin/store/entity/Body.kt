package com.kotlin.store.entity

data class Body(
    var from: String,
    var to: String,
    var subject: String?,
    var text: String?
)
