package com.kotlin.store.entity

data class Person(
    var id: Int?,
    var personType: PersonType,
    var name: String,
    var documentType: String,
    var documentNumber: Int,
    var address: String,
    var phone: Int,
    var email: String
)
