package com.kotlin.store.config

import com.kotlin.store.events.Event
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core.ConsumerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.support.serializer.JsonDeserializer
import org.springframework.kafka.support.serializer.JsonSerializer


@EnableKafka
@Configuration
class KafkaConsumerConfig {

    @Value("\${kafka.bootstrap.address}")
    private lateinit var bootstrapAddress: String

        @Bean
        fun consumerFactory(): ConsumerFactory<String, Event> {
            val props = mutableMapOf<String, Any>()
            props[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = bootstrapAddress
            props[JsonSerializer.TYPE_MAPPINGS] = "com.kotlin:com.kotlin.store.events.Event"
            val jsonDeserializer: JsonDeserializer<Event> = JsonDeserializer()
            return DefaultKafkaConsumerFactory(
                props,
                StringDeserializer(),
                jsonDeserializer)
        }

    @Bean
    fun kafkaListenerContainerFactory(): ConcurrentKafkaListenerContainerFactory<String, Event>? {
        val factory: ConcurrentKafkaListenerContainerFactory<String, Event> =
            ConcurrentKafkaListenerContainerFactory<String, Event>()
        factory.consumerFactory = consumerFactory()
        return factory
    }
}