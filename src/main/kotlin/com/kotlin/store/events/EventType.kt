package com.kotlin.store.events

enum class EventType {
    CREATED, UPDATED, DELETED
}