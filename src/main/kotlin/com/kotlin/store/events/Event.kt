package com.kotlin.store.events

import com.kotlin.store.entity.Person
import java.util.Date

data class Event(
    var id: String,
    var date: Date,
    var type: EventType,
    var data: Person
)
