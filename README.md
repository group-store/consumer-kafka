## Kafka consumer application

Perform the following steps to run the app:

1. Change the environment variables for gmail user and password in the yml configuration file
2. Run the application with the "gradle bootRun" command
